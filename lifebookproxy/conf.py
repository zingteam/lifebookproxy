import dj_database_url
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# ENV = 'local'
ENV = 'prod'

#############################
#   BRANCH CONFIGURATIONS   #
#############################

# Configurations for 'LOCAL'
LOCAL_CONF = {
    'DATABASES': {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24 * 30 * 2,  # Cookie age: about 2 months, in seconds
    'CSRF_SECURE': False,  # False disables csrf control when developing in local
    'DEBUG': True,
    'ALLOWED_HOSTS': ['*'],

}


# Configurations for 'PRODUCTION'
PROD_CONF = {
    # The actual Cloud SQL ip and user auth coordinates: use when in need to syncdb, shell
    'DATABASES': {
        'default': dj_database_url.config()
    },

    'SESSION_COOKIE_AGE': 60 * 60 * 24,  # Cookie age: a day in seconds
    'CSRF_SECURE': True,
    'DEBUG': True,
    'ALLOWED_HOSTS': ['*'],

}


# Configurations dicts mapping
CONF_DICT = {
    'local': LOCAL_CONF,
    'prod': PROD_CONF,
}

# Edit dict key to select the current branch
CONF = CONF_DICT['local']